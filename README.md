sudo picocom -b 115200 /dev/ttyUSB0
rescue image:
user:password = root:root

This is what I ordered:

PICOPIIMX8MR10

PICO-PI-IMX8M-BASIC

570-PICOPI-DEV-BASIC

Development Boards & Kits - ARM PICO-PI-IMX8M-BASIC

570-CAMOV5645

#### 28.7.2021 IMPORTANT UPDATE!

The debugs console is the micro-usb entrence and not the pinout!
The pinout is in the schematics file. Look for J8.

I compiled the ~/git/media_tree repo with:
```
export O=../kbuild/picopi
export KBUILD_OUTPUT=../kbuild/picopi
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64
```
This is in `picopi.sh` in the ~/git/media_tree.
When booting you should click space continousaly so to get to the u-boot console.
Then what seems to work is:
```
u-boot=> setenv serverip 10.42.0.1
u-boot=> setenv ipaddr 10.42.0.2
u-boot=> setenv fdt_addr 0x43000000
u-boot=> setenv bootargs  root=/dev/nfs ip=dhcp nfsroot=${serverip}:/srv/nfs/debian/testing,tcp,v3 nfsrootgv ro
u-boot=> tftpboot ${loadaddr} boot/pico-image; tftpboot ${fdt_addr} boot/fdtdir/picopi/imx8mq-pico-pi.dtb; booti ${loadaddr} - ${fdt_addr}
```
The tftp config is:
```
root@guri:/home/dafna/git/picopi# cat /srv/tftp/pxelinux.cfg/default-pico
default l0
menu title U-Boot menu
prompt 0
timeout 50

label l0
        menu label Pico Pi (Debian/Testing)
        linux /boot/Image
        fdt /boot/fdtdir/picopi/imx8mq-pico-pi.dtb
        append loglevel=8 root=/dev/nfs ip=10.42.0.2 rootwait rw earlyprintk nfsroot=10.42.0.1:/srv/nfs/debian/testing,v3
```



